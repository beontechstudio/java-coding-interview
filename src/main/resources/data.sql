insert into FLIGHT(id, code, origin, destination, status) values (1, 'FL10', 'GRU', 'ATL', 'confirmed');
insert into FLIGHT(id, code, origin, destination, status) values (2, 'FL11', 'DXB', 'LAX', 'confirmed');
insert into FLIGHT(id, code, origin, destination, status) values (3, 'FL12', 'IST', 'LHR', 'cancelled');
insert into FLIGHT(id, code, origin, destination, status) values (4, 'FL13', 'DEL', 'DEL', 'confirmed');
insert into FLIGHT(id, code, origin, destination, status) values (5, 'FL14', 'AMS', 'MAD', 'confirmed');
